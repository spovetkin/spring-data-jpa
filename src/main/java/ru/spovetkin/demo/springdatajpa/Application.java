package ru.spovetkin.demo.springdatajpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.spovetkin.demo.springdatajpa.database.entity.User;
import ru.spovetkin.demo.springdatajpa.database.repository.UserRepository;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class Application {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    @Autowired
    private UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @PostConstruct
    private void postConstruct() {
        userRepository.save(new User("spovetkin", "Sergey", "Povetkin"));
        userRepository.save(new User("sivanov", "Sergey", "Ivanov"));
        userRepository.save(new User("ekhodosov", "Evgeniy", "Khodosov"));

//        userRepository.findByLogin("ekhodosov").ifPresent(u ->logger.info(u.toString()));
        userRepository.findByFirstName("Sergey").forEach(u-> logger.info(u.toString()));
    }
}
