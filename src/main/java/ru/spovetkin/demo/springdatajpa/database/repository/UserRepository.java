package ru.spovetkin.demo.springdatajpa.database.repository;

import org.springframework.data.repository.CrudRepository;
import ru.spovetkin.demo.springdatajpa.database.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByLogin(String login);

    List<User> findByFirstName(String firstName);

}
